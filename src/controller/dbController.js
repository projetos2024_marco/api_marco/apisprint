const connect = require("../db/connect"); // Importa o módulo de conexão com o banco de dados

module.exports = class dbController {
    static async getNameTables(req, res) {
        const queryShowTables = "SHOW TABLES"; // Define a consulta SQL para obter os nomes das tabelas

        // Executa a consulta SQL para obter os nomes das tabelas
        connect.query(queryShowTables, function(err, result, fields) {
            if (err) { // Verifica se houve algum erro durante a consulta
                console.log('Erro ao obter as tabelas:', err); // Registra o erro no console
                return res.status(500).json({ error: "Erro ao obter tabelas do banco de dados" }); // Retorna uma resposta de erro 500 em formato JSON
            }

            // Extrai os nomes das tabelas do resultado da consulta e envia uma resposta com os nomes em formato JSON
            const tableNames = result.map(row => row[Object.keys(row)[0]]);
            res.status(200).json({ tables: tableNames });
        });
    }

    static async getTablesDescription(req, res) {
        const queryShowTables = "SHOW TABLES"; // Define a consulta SQL para obter os nomes das tabelas

        // Executa a consulta SQL para obter os nomes das tabelas, utilizando async/await para trabalhar com a resposta
        connect.query(queryShowTables, async function(err, result, fields) {
            if (err) { // Verifica se houve algum erro durante a consulta
                console.log('Erro ao obter as tabelas:', err); // Registra o erro no console
                return res.status(500).json({ error: "Erro ao obter tabelas do banco de dados" }); // Retorna uma resposta de erro 500 em formato JSON
            }

            const tables = []; // Inicializa um array para armazenar as descrições das tabelas

            // Itera sobre cada tabela no resultado da consulta
            for (let i = 0; i < result.length; i++) {
                const tableName = result[i][Object.keys(result[i])[0]]; // Obtém o nome da tabela atual
                const queryDescTable = `DESCRIBE ${tableName}`; // Constrói a consulta para obter a descrição da tabela atual

                try {
                    // Executa a consulta para obter a descrição da tabela atual usando uma Promise e await
                    const tableDescription = await new Promise((resolve, reject) => {
                        connect.query(queryDescTable, function(err, result, fields) {
                            if (err) {
                                reject(err); // Rejeita a Promise em caso de erro
                            }
                            resolve(result); // Resolve a Promise com o resultado da consulta
                        });
                    });

                    // Adiciona o nome e a descrição da tabela ao array de tabelas
                    tables.push({ name: tableName, description: tableDescription });
                } catch (error) {
                    console.log(error); // Registra o erro no console em caso de falha na obtenção da descrição da tabela
                    return res.status(500).json({ error: "Erro ao obter a descrição da tabela!" }); // Retorna uma resposta de erro 500 em formato JSON
                }
            }

            // Envia uma resposta de sucesso com as descrições das tabelas em formato JSON
            res.status(200).json({ tables });
        });
    }
};