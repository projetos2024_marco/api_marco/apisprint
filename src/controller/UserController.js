module.exports = class UserController {
  static async postUser(req, res) {
    const { nome, senha, email, telefone } = req.body;
    

    if (!nome || !senha || !email || !telefone) {
      return res
        .status(400)
        .json({ message: "Campos obrigatórios não preenchidos" });
    }

    if (senha.length < 5) {
      return res
        .status(400)
        .json({ message: "Sua senha precisa ter no mínimo 5 digitos." });
    }

    const simbolosEmail = /@/;

    if (!simbolosEmail.test(email)) {
      return res.status(400).json({ message: "email inválido." });
    }

    // Validar se o email possui um dos domínios específicos (.com, .io, .net)
    const dominiosPermitidos = /\.(com|io|net|br)$/i;
    if (!dominiosPermitidos.test(email)) {
      return res.status(400).json({
        message: "O email deve ter um dos domínios permitidos: .com, .io, .net",
      });
    }

    // Validar se o telefone contém apenas números
    const telefoneNumerico = /^[0-9]+$/;
    if (!telefoneNumerico.test(telefone)) {
      return res
        .status(400)
        .json({ message: "O telefone deve conter apenas números." });
    }

    // Validar o comprimento máximo do telefone
    const tamanhoMaximoTelefone = 15; // Altere conforme necessário
    if (telefone.length > tamanhoMaximoTelefone) {
      return res.status(400).json({
        message: `O telefone deve ter no máximo ${tamanhoMaximoTelefone} caracteres.`,
      });
    }

    res.status(200).json({ message: "Usuário cadastrado com sucesso." });
    console.log("Usuário cadastrado:", req.body);
  }

  static async updateUser(req, res) {
    const { nome, senha, email, telefone } = req.body;

    if (!nome || !senha || !email || !telefone) {
      return res
        .status(400)
        .json({ message: "Campos obrigatórios não preenchidos" });
    }

    if (senha.length < 5) {
      return res
        .status(400)
        .json({ message: "Sua senha precisa ter no mínimo 5 digitos." });
    }

    const simbolosEmail = /@/;

    if (!simbolosEmail.test(email)) {
      return res.status(400).json({ message: "Sua email precisa ser válido." });
    }

    // Validar se o email possui um dos domínios específicos (.com, .io, .net)
    const dominiosPermitidos = /\.(com|io|net)$/i;
    if (!dominiosPermitidos.test(email)) {
      return res.status(400).json({
        message: "O email deve ter um dos domínios permitidos: .com, .io, .net",
      });
    }

    // Validar se o telefone contém apenas números
    const telefoneNumerico = /^[0-9]+$/;
    if (!telefoneNumerico.test(telefone)) {
      return res
        .status(400)
        .json({ message: "O telefone deve conter apenas números." });
    }

    // Validar o comprimento máximo do telefone
    const tamanhoMaximoTelefone = 15; // Altere conforme necessário
    if (telefone.length > tamanhoMaximoTelefone) {
      return res.status(400).json({
        message: `O telefone deve ter no máximo ${tamanhoMaximoTelefone} caracteres.`,
      });
    }

    res.status(200).json({ message: "Usuário atualizado com sucesso." });
    console.log("Usuário atualizado:", req.body);
  }

  static async deleteUser(req, res) {
    const { id } = req.params;

    if (!id) {
      return res.status(400).json({ message: "Erro ao deletar" });
    }

    return res
      .status(200)
      .json({ message: "O usuário foi deletado com sucesso" });
  }

  static async getUser(req, res) {
    res.status(200).json({
      algumaMensagem: {
        algumaMensagem: "Lista",
        algumNúmero: 69,
      },
    });
  }


  static async loginUser(req, res) {
    const { senha, email } = req.body;

 
    if (email === 'gabriel@gmail' && senha === '12345') {
        res.status(200).json({ message: "Email logado com sucesso." });
    }
     else {
        res.status(401).json({ message: "Email ou senha incorretos." });
    }
}

};
