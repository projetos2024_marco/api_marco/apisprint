module.exports = class QuadraController {
    // Método para criar uma nova quadra esportiva
    static async postQuadra(req, res) {
        const { TipoQuadra, Capacidade, ValorPeriodo } = req.body;

        // Verifica se todos os campos obrigatórios foram preenchidos
        if (!TipoQuadra || !Capacidade || !ValorPeriodo) {
            return res
                .status(400)
                .json({ message: "Campos obrigatórios não preenchidos" });
        }

        // Verifica se o valor por período é válido ("Manhã", "Tarde" ou "Noite")
        if (ValorPeriodo !== "Manhã" && ValorPeriodo !== "Tarde" && ValorPeriodo !== "Noite") {
            return res
                .status(400)
                .json({ message: "O valor por período deve ser 'Manhã', 'Tarde' ou 'Noite'." });
        }

        // Se todas as validações passarem, retorna sucesso e registra a nova quadra
        res.status(200).json({ message: "Quadra cadastrada com sucesso." });
        console.log("Quadra cadastrada:", req.body);
    }

    // Método para atualizar uma quadra existente
    static async updateQuadra(req, res) {
        const { TipoQuadra, Capacidade, ValorPeriodo } = req.body;

        // Verifica se todos os campos obrigatórios foram preenchidos
        if (!TipoQuadra || !Capacidade || !ValorPeriodo) {
            return res
                .status(400)
                .json({ message: "Campos obrigatórios não preenchidos" });
        }

        // Verifica se o valor por período é válido ("Manhã", "Tarde" ou "Noite")
        if (ValorPeriodo !== "Manhã" && ValorPeriodo !== "Tarde" && ValorPeriodo !== "Noite") {
            return res
                .status(400)
                .json({ message: "O valor por período deve ser 'Manhã', 'Tarde' ou 'Noite'." });
        }

        // Se todas as validações passarem, retorna sucesso e registra a atualização da quadra
        res.status(200).json({ message: "Quadra atualizada com sucesso." });
        console.log("Quadra atualizada:", req.body);
    }

    // Método para excluir uma quadra pelo ID
    static async deleteQuadraId(req, res) {
        const { id } = req.params;

        try {
            // Verifica se o ID não foi fornecido, é NaN ou menor ou igual a zero
            if (!id || isNaN(id) || parseInt(id) <= 0) {
                throw new Error("ID de Quadra inválido!");
            }

            // Retornar uma resposta de sucesso com o ID da quadra que foi removida
            return res.status(200).json({ message: "Quadra removida com ID " + id });
        } catch (error) {
            // Lidar com erros durante a exclusão da quadra
            console.error("Erro ao excluir Quadra:", error.message);
            return res.status(400).json({ message: error.message });
        }
    }

    // Método para obter informações de várias quadras
    static async getQuadra(req, res) {
        try {
            // Retorna informações fictícias de várias quadras
            res.status(200).json({
                quadras: [
                    {
                        Capacidade: 22,
                        TipoQuadra: "Futebol",
                        ValorPeriodo: "Tarde"
                    },
                    {
                        Capacidade: 10,
                        TipoQuadra: "Basquete",
                        ValorPeriodo: "Tarde"
                    },
                    {
                        Capacidade: 12,
                        TipoQuadra: "Volei",
                        ValorPeriodo: "Manhã"
                    },
                    {
                        Capacidade: 4,
                        TipoQuadra: "Tênis",
                        ValorPeriodo: "Manhã"
                    },
                ],
            });
        } catch (error) {
            console.error("Erro ao obter informações de quadra:", error.message);
            res.status(400).json({ message: "Erro ao obter informações de quadra." });
        }
    }
};
