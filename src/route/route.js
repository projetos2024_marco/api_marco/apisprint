const router = require("express").Router(); // Importa o módulo Router do Express
//const LoginController = require('../controller/LoginController')
const userController = require('../controller/UserController')
const dbController = require("../controller/dbController"); // Importa o controlador dbController
const quadraController = require("../controller/quadraController");// Importa o controlador quadraController




//Rotas do quadraController
router.post('/quadra/', quadraController.postQuadra)
router.put('/quadra/', quadraController.updateQuadra)
router.delete('/quadra/:id', quadraController.deleteQuadraId)
router.get('/quadra/', quadraController.getQuadra)

//Rotas do dbController
router.post('/usuario/', userController.postUser)
router.put('/usuario/', userController.updateUser)
router.delete('/usuario/:id', userController.deleteUser)
router.get('/usuario/', userController.getUser)
router.post('/login/',userController.loginUser)

// Define rotas para consultar tabelas e descrições de tabelas
router.get("/tables", dbController.getNameTables); // Rota para consultar os nomes das tabelas do banco de dados
router.get('/tablesdescription', dbController.getTablesDescription); // Rota para consultar as descrições das tabelas do banco de dados

module.exports = router; // Exporta o objeto router com as rotas definidas
